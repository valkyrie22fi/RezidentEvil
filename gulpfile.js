const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const gcmq = require('gulp-group-css-media-queries');
const imagemin = require('gulp-imagemin');
const pug = require('gulp-pug');


const config = {
    src: {
        root: './src',
        scss: '/scss',
        img: '/img',
        watch: '/scss/**/*.scss'
    },
    dist: {
        root: './dist',
        css: '/css'
    },
    html: '/*.html'
};

// Watcher
gulp.task('watch', ['browser-sync'], function () {
    gulp.watch(config.src.root + config.src.watch, ['build-css']);
    gulp.watch(config.src.root + '/pug/**/*.pug', ['build-html']);
    gulp.watch(config.src.root + '/fonts/**/*', ['build-fonts']);
    gulp.watch(config.src.root + '/js/**/*', ['build-js']);
    gulp.watch(config.src.root + '/img/**/*', ['imagemin']);
    gulp.watch(config.dist.root + config.html).on('change', browserSync.reload);
    gulp.watch(config.dist.root + '/js/*.js').on('change', browserSync.reload);
});

// Browser-sync
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: config.dist.root
        }
    });
});

// Css
gulp.task('build-css', function() {
    gulp.src(config.src.root + config.src.scss + '/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['> 0.1%'],
            cascade: false
        }))
        .pipe(cleanCSS())
        .pipe(gcmq())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.dist.root + config.dist.css))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// Pug
gulp.task('build-html', function buildHTML() {
    return gulp.src(config.src.root + '/pug/*.pug')
        .pipe(pug())
        .pipe(gulp.dest(config.dist.root));
});

// Imagemin
gulp.task('imagemin', function() {
    gulp.src('src/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
});

// Fonts
gulp.task('build-fonts', function() {
    return gulp.src(['src/fonts/**/*'])
        .pipe(gulp.dest('dist/fonts/'));
});

// Javascript
gulp.task('build-js', function() {
    return gulp.src(['src/js/**/*'])
        .pipe(gulp.dest('dist/js/'));
});

// Video
gulp.task('build-video', function() {
    return gulp.src(['src/video/**/*'])
        .pipe(gulp.dest('dist/video/'));
});