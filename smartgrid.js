var smartgrid = require('smart-grid');

var settings = {
    outputStyle: 'scss', /* less || scss || sass || styl */
    columns: 12, /* number of grid columns */
    offset: '0px', /* gutter width px || % */
    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
    oldSizeStyle: false,
    container: {
        maxWidth: '1368px', /* max-width оn very large screen */
        fields: '100px' /* side fields */
    },
    breakPoints: {
        md: {
            width: '1023px',
            fields: '50px'
        },
        sm: {
            width: '768px',
            fields: '0px'
        }
    }
};

smartgrid('./src/scss', settings);