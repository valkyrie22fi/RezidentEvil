$(document).ready(function(){
    $('.info-slider').slick({
        dots: false,
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1
    });
    $('.tabs-slider').slick({
        dots: false,
        arrows: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1020,
                settings: {
                    slidesToShow: 4,
                    arrows: true,
                    infinite: true
                }
            },
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
    $('.slider-for-tabs').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        fade: true,
        asNavFor: '.slider-nav-tabs'
    });
    $('.slider-nav-tabs').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider-for-tabs',
        arrows: false,
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        variableWidth: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4,
                    arrows: false,
                    dots: false,
                    variableWidth: true,
                    centerMode: false,
                    centerPadding: '40px'
                }
            }
        ]
    });
});